<!-- Directory Application
	 Author: Daniel dbm0100@yahoo.com
	 A basic directory application with
	 create, read, update, delete functionality
	 Version 0.1 completed 11-27.
	 Hope to revisit this and improve UI and
	 implement more AJAX!-->
<!DOCTYPE html>
<html lang = "en">
    <head>
        <meta charset="utf-8"/>
        <title>Daniel Meakin | Portfolio</title>
        <link rel = "stylesheet" href = "../../main.css"/>
        <link rel = "stylesheet" href = "style.css"/>
        <script language ="javascript" type ="text/javascript" src ="script.js"></script>

    </head>
    <body>
        <div id = "big_wrapper">
            <header id = "top_header">
                <h1>Daniel Meakin | Portfolio</h1>            
            </header>
            <nav id = "top_menu">
                <ul>
                    <li><a href = "../../index.html">Home</a></li>
                    <li><a href = "../../resume.html">Resume</a></li>
                    <li><a href = "../../portfolio/">Portfolio</a></li>
                </ul>
            </nav>
            <section id = "section_main">
                <h2>Persons Listing</h2>
    			<!-- A search menu for searching the records -->
    			<form name = "search" onsubmit ="return showRecord();">
    				<input type ="button" onclick ="showRecord()" value ="Search"/>
    				<select id = "searchMenu" name = "searchMenu">
    					<option value="lastName">Last Name</option>
    					<option value="firstName">First Name</option>
    					<option value="dob">Date of Birth</option>
    					<option value="zip">Zip Code</option>
    				</select>
    				<input type ="text" id = "searchValue" name ="searchValue" size = "11"/>
    			</form><br/>
    
    			<!-- Here's where our search results will end up -->
    			<div id ="searchResults"></div>
    
    			<!-- This form is used for editing, deleting, adding records to the databse -->
     			<form name ="personsForm"action ="index.php" method ="post" onsubmit="return validateForm();" >
    				<table id ="mainTable">
    					<tr>
    						<td>
    							<table id="tableHead">
    								<tr>
    									<th>Last Name</th>
    									<th>First Name</th>
    									<th>Date Birth</th>
    									<th>Zip Code</th>
    								</tr>
    							</table>
    						</td>
    					</tr>
    					<tr>
    						<td>
    							<div id ="scrollingTable">
    								<table id ="innerTable">
<?php 
// set variables used for $conn
require_once('dbInfo.php');

// Create Connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check Connection
if ($conn->connect_error) {
	die ("Connection failed: " . $conn->connect_error);
}

// This if statement handles different cases for contents of POST
if ( empty($_POST))
	$sql = "SELECT * FROM persons";

// Submit button case Inserts new record before display
elseif ($_POST["insert"] ===""){
	$sql = "INSERT INTO persons (lastName, firstName, dob, zip) VALUES ('".$_POST[lastName]."', '".$_POST[firstName]."', '".$_POST[dob]."', '".$_POST[zip]."');";
	$conn->query($sql);
	$sql = "SELECT * FROM persons";
}

// Delete button cases, deletes record before display
elseif ($_POST["delete"] ===""){
	$sql = "DELETE FROM persons WHERE pri_key='".$_POST["pri_key"]."'";
	$conn->query($sql);
	$sql = "SELECT * FROM persons";
}

// Update button case, updates record
elseif ($_POST["update"] ===""){
	$sql = "UPDATE persons SET lastName = '".$_POST["lastName"]."', firstName = '".$_POST["firstName"]."', dob = '".$_POST["dob"]."', zip = '".$_POST["zip"]."' WHERE persons.pri_key = ".$_POST["pri_key"];
	$conn->query($sql);
	echo $conn->error;
	$sql = "SELECT * FROM persons";
}

$result = $conn->query($sql);

if ($result->num_rows > 0){
	//output data of each row
	$i=0;
	while ($row = $result->fetch_assoc()){
		echo "<tr><td>".$row["lastName"]."</td><td>".$row["firstName"].
			"</td><td>".$row["dob"]."</td><td>".$row["zip"]."</td>".
            "<td class = 'pri_key'>".$row["pri_key"]."</td>".
			"<td class = 'buttonField'><input type = 'button' class = 'togglee' onclick='edit(".$row['pri_key'].");' value = 'edit'/></td>\n".
			"<td class = 'buttonField'><button type = 'submit' class = 'togglee' name = 'delete' onclick = 'deleteIt(".$row['pri_key'].");'>delete</button></td></tr>\n";
		$i++;
	}
}
$conn->close();

?>
    								</table>
    							</div>
    						</td>
    					</tr>
    					<tr>
    						<td>
    							<!-- this table used for creating a new record -->
    							<table id="newTable">
    								<tr class ="inputRow">
    									<td><input type ="button" value ="new" onclick ="newRecord();"/></td>
    									<td></td>
    									<td></td>
    									<td></td>
    									<td class ="pri_key"> </td>
    									<td></td>
    									<td></td>
                                        <td></td>
    								</tr>
    							</table>
    						</td>
    					</tr>
    				</table>
    			</form>
            </section>
        </div>
    </body>
</html>
